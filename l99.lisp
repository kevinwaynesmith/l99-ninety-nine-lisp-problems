(asdf:load-system :trivia)
(asdf:load-system :alexandria)

(defpackage :l99
  (:import-from :trivia #:match #:guard)
  (:use :cl)
  (:local-nicknames (:al :alexandria)))

(in-package :l99)

;; helper functions
(defun foldl (fn value list)
  (match list
    (nil value)
    ((cons x xs) (foldl fn (funcall fn value x) xs))))

(defun take-while (list fn)
  (labels ((aux (acc rem)
	     (match rem
	       (nil (nreverse acc))
	       ((cons x xs)
		(if (funcall fn x)
		    (aux (cons x acc) xs)
		    (nreverse acc))))))
    (aux nil list)))

;;;; Lists

;; P01 (*) Find the last box of a list.
;; Example:
;; * (my-last '(a b c d))
;; (D)

(defun my-last (list)
  (match list
    (nil nil)
    ((cons x nil) x)
    ((cons _ xs) (my-last xs))))

;; P02 (*) Find the last but one box of a list.
;; Example:
;; * (my-but-last '(a b c d))
;; (C D)

(defun my-but-last (list)
  (match (cdr list)
    (nil nil)
    ((cons _ nil) (car list))
    ((cons _ xs) (my-but-last (cdr list)))))

;; P03 (*) Find the K'th element of a list.
;; The first element in the list is number 1.
;; Example:
;; * (element-at '(a b c d e) 3)
;; C

(defun element-at (lst i)
  (if (< i 1)
      (car lst)
      (element-at (cdr lst) (1- i))))

;; P04 (*) Find the number of elements of a list.

(defun my-length (list)
  (foldl (lambda (acc _) (1+ acc)) 0 list))

;; P05 (*) Reverse a list.

(defun my-reverse (list)
  (foldl (lambda (acc x) (cons x acc)) nil list))

;; P06 (*) Find out whether a list is a palindrome.
;;     A palindrome can be read forward or backward; e.g. (x a m a x).

(defun palindromep (lst)
  (equal lst (my-reverse lst)))

;; P07 (**) Flatten a nested list structure.
;; Transform a list, possibly holding lists as elements into a `flat' list by replacing each list with its elements (recursively).
;;
;; Example:
;; * (my-flatten '(a (b (c d) e)))
;; (A B C D E)
;;
;; Hint: Use the predefined functions list and append.

(defun my-flatten (list)
  (match list
    (nil nil)
    ((guard (cons x xs) (atom x))
     (cons x (my-flatten xs)))
    ((cons xs xss)
     (append (my-flatten xs) (my-flatten xss)))))

;; P08 (**) Eliminate consecutive duplicates of list elements.
;; If a list contains repeated elements they should be replaced with a single copy of the element. The order of the elements should not be changed.
;;
;; Example:
;; * (compress '(a a a a b c c a a d e e e e))
;; (A B C A D E)

(defun compress (lst)
  (flet ((aux (acc e)
	   (if (equal e (car acc))
	       acc
	       (cons e acc))))
    (my-reverse (foldl #'aux nil lst))))

;; P09 (**) Pack consecutive duplicates of list elements into sublists.
;; If a list contains repeated elements they should be placed in separate sublists.
;;
;; Example:
;; * (pack '(a a a a b c c a a d e e e e))
;; ((A A A A) (B) (C C) (A A) (D) (E E E E))

(defun pack (list)
  (labels ((aux (acc chars rem)
	     (match (list chars rem)
	       ((list _ nil)
		(my-reverse (cons chars acc)))
	       ((guard (list (cons x xs) (cons y ys))
		       (equal x y))
		(aux acc (cons x chars) ys))
	       ((list chars (cons y ys))
		(aux (cons chars acc) (list y) ys)))))
    (match list
      (nil nil)
      ((cons y ys)
       (aux nil (list y) ys)))))

;; P10 (*) Run-length encoding of a list.
;; Use the result of problem P09 to implement the so-called run-length encoding data compression method. Consecutive duplicates of elements are encoded as lists (N E) where N is the number of duplicates of the element E.
;;
;; Example:
;; * (encode '(a a a a b c c a a d e e e e))
;; ((4 A) (1 B) (2 C) (2 A) (1 D)(4 E))

(defun encode (list)
  (mapcar (lambda (x) (list (length x) (car x)))
	  (pack list)))

;; P11 (*) Modified run-length encoding.
;; Modify the result of problem P10 in such a way that if an element has no duplicates it is simply copied into the result list. Only elements with duplicates are transferred as (N E) lists.
;;
;; Example:
;; * (encode-modified '(a a a a b c c a a d e e e e))
;; ((4 A) B (2 C) (2 A) D (4 E))

(defun encode-modified (list)
  (mapcar (lambda (x)
	    (let ((length (length x))
		  (head (car x)))
	      (if (equal length 1)
		  head
		  (list length head))))
	  (pack list)))

;; P12 (**) Decode a run-length encoded list.
;; Given a run-length code list generated as specified in problem P11. Construct its uncompressed version.

(defun decode (list)
  (let (acc '())
    (mapc (lambda (x)
	    (if (atom x)
		(push x acc)
		(dotimes (_ (car x))
		  (push (cadr x) acc))))
	  list)
    (my-reverse acc)))

;; P13 (**) Run-length encoding of a list (direct solution).
;; Implement the so-called run-length encoding data compression method directly. I.e. don't explicitly create the sublists containing the duplicates, as in problem P09, but only count them. As in problem P11, simplify the result list by replacing the singleton lists (1 X) by X.
;;
;; Example:
;; * (encode-direct '(a a a a b c c a a d e e e e))
;; ((4 A) B (2 C) (2 A) D (4 E))

(defun encode-direct (list)
  (labels ((aux (element count acc rem)
	     (match rem
	       (nil (if (equal count 1)
			(cons element acc)
			(cons (list count element) acc)))
	       ((cons x xs)
		(cond ((equal element x) (aux element (1+ count) acc xs))
		      ((equal count 1) (aux x 1 (cons element acc) xs))
		      (t (aux x 1 (cons (list count element) acc) xs)))))))
    (match list
      (nil nil)
      ((cons x xs)
       (nreverse (aux x 1 nil xs))))))

;; P14 (*) Duplicate the elements of a list.
;; Example:
;; * (dupli '(a b c c d))
;; (A A B B C C C C D D)

(defun dupli (list)
  (flet ((aux (acc e)
	   (cons e (cons e acc))))
    (nreverse (foldl #'aux nil list))))

;; P15 (**) Replicate the elements of a list a given number of times.
;; Example:
;; * (repli '(a b c) 3)
;; (A A A B B B C C C)

(defun repli (list times)
  (al:mappend (lambda (x)
		(let (acc '())
		  (dotimes (_ times)
		    (push x acc))
		  acc))
	      list))

;; P16 (**) Drop every N'th element from a list.
;; Example:
;; * (drop '(a b c d e f g h i k) 3)
;; (A B D E G H K)

(defun drop (list n)
  (cond ((<= n 0) list)
	((= n 1) (cdr list))
	(t (let ((count 1))
	     (flet ((aux (acc x)
		      (if (equal count n)
			  (progn (setf count 1)
				 acc)
			  (progn (setf count (1+ count))
				 (cons x acc)))))
	       (my-reverse (foldl #'aux nil list)))))))

;; P17 (*) Split a list into two parts; the length of the first part is given.
;; Do not use any predefined functions.
;;
;; Example:
;; * (split '(a b c d e f g h i k) 3)
;; ( (A B C) (D E F G H I K))

(defun split (list n)
  (labels ((aux (acc rem count)
	     (match rem
	       (nil (list (list)))
	       ((cons x xs)
		(if (equal count n)
		    (list (my-reverse acc) rem)
		    (aux (cons x acc) xs (1+ count)))))))
    (aux nil list 0)))

;; P18 (**) Extract a slice from a list.
;; Given two indices, I and K, the slice is the list containing the elements between the I'th and K'th element of the original list (both limits included). Start counting the elements with 1.
;;
;; Example:
;; * (slice '(a b c d e f g h i k) 3 7)
;; (C D E F G)

(defun slice (list i k)
  (labels ((aux (idx acc rem)
	     (match rem
	       (nil acc)
	       ((cons x xs) (cond ((> i idx) (aux (1+ idx) acc xs))
				  ((and (<= i idx) (> k idx))
				   (aux (1+ idx) (cons x acc) xs))
				  ((= k idx) (cons x acc)))))))
    (unless (>= i k)
      (my-reverse (aux 1 nil list)))))

;; P19 (**) Rotate a list N places to the left.
;; Examples:
;; * (rotate '(a b c d e f g h) 3)
;; (D E F G H A B C)
;;
;; * (rotate '(a b c d e f g h) -2)
;; (G H A B C D E F)

(defun rotate (list n)
  (cond ((> n 0) (append (subseq list n) (subseq list 0 n)))
	((< n 0) (let ((border (+ (my-length list) n)))
		   (append (subseq list border) (subseq list 0 border))))
	(t list)))

;; P20 (*) Remove the K'th element from a list.
;; Example:
;; * (remove-at '(a b c d) 2)
;; (A C D)

(defun remove-at (list k)
  (labels ((aux (acc rem idx)
	     (match rem
	       (nil (nreverse acc))
	       ((cons x xs) (if (= idx k)
				(values (append (nreverse acc) xs) x)
				(aux (cons x acc) xs (1+ idx)))))))
    (aux nil list 1)))

;; P21 (*) Insert an element at a given position into a list.
;; Example:
;; * (insert-at 'alfa '(a b c d) 2)
;; (A ALFA B C D)

(defun insert-at (e list i)
  (labels ((aux (acc rem idx)
	     (match rem
	       (nil (if (= idx i)
			(nreverse (cons e acc))
			(error "index out of bounds")))
	       ((cons x xs) (if (= idx i)
				(append (nreverse (cons e acc)) rem)
				(aux (cons x acc) xs (1+ idx)))))))
    (aux nil list 1)))

;; P22 (*) Create a list containing all integers within a given range.
;; If first argument is smaller than second, produce a list in decreasing order.
;; Example:
;; * (range 4 9)
;; (4 5 6 7 8 9)

(defun range (i j)
  (labels ((aux (acc idx)
	     (if (= idx j)
		 (nreverse (cons idx acc))
		 (aux (cons idx acc) (1+ idx)))))
    (if (<= i j)
	(aux nil i)
	(nreverse (range j i)))))

;; P23 (**) Extract a given number of randomly selected elements from a list.
;; The selected items shall be returned in a list.
;; Example:
;; * (rnd-select '(a b c d e f g h) 3)
;; (E D A)
;;
;; Hint: Use the built-in random number generator and the result of problem P20.

(defun rnd-select (list n)
  (let ((max (my-length list)))
    (labels ((aux (acc rem count)
	       (if (= count n)
		   acc
		   (multiple-value-bind (xs x)
		       (remove-at rem (1+  (random (- max count))))
		     (aux (cons x acc) xs (1+ count))))))
      (aux nil list 0))))

;; P24 (*) Lotto: Draw N different random numbers from the set 1..M.
;; The selected numbers shall be returned in a list.
;; Example:
;; * (lotto-select 6 49)
;; (23 1 17 33 21 37)
;;
;; Hint: Combine the solutions of problems P22 and P23.

(defun lotto-select (n m)
  (rnd-select (range 1 m) n))

;; P25 (*) Generate a random permutation of the elements of a list.
;; Example:
;; * (rnd-permu '(a b c d e f))
;; (B A D C E F)
;;
;; Hint: Use the solution of problem P23.

(defun rnd-permu (list)
  (rnd-select list (my-length list)))



;;;; Arithmetic
;; P31 (**) Determine whether a given integer number is prime.
;; Example:
;; * (is-prime 7)
;; T

;; trial division
(defun is-prime (n)
  (if (< n 2)
      nil
      (progn (loop for i from 2 to (isqrt n)
		   do (if (zerop (mod n i))
			  (return-from is-prime nil)))
	     t)))

;; P32 (**) Determine the greatest common divisor of two positive integer numbers.
;; Use Euclid's algorithm.
;; Example:
;; * (gcd 36 63)
;; 9

(defun my-gcd (a b)
  (cond ((zerop a) b)
	((zerop b) a)
	(t (gcd b (mod a b)))))

;; P33 (*) Determine whether two positive integer numbers are coprime.
;; Two numbers are coprime if their greatest common divisor equals 1.
;; Example:
;; * (coprime 35 64)
;; T

(defun coprime (n m)
  (= (gcd n m) 1))

;; P34 (**) Calculate Euler's totient function phi(m).
;; Euler's so-called totient function phi(m) is defined as the number of positive integers r (1 <= r < m) that are coprime to m.
;;
;; Example: m = 10: r = 1,3,7,9; thus phi(m) = 4. Note the special case: phi(1) = 1.
;;
;; * (totient-phi 10)
;; 4
;;
;; Find out what the value of phi(m) is if m is a prime number. Euler's totient function plays an important role in one of the most widely used public key cryptography methods (RSA). In this exercise you should use the most primitive method to calculate this function (there are smarter ways that we shall discuss later).

(defun totient-phi (n)
  (if (= n 1)
      1
      (my-length (remove-if-not (lambda (x) (coprime n x))
				(range 1 (1- n))))))

(defun totient-phi-iterative (n)
  (if (= n 1)
      1
      (let ((count 0))
	(progn
	  (dotimes (i n)
	    (when (coprime (1+ i) n)
	      (setq count (1+ count))))
	  count))))

;; P35 (**) Determine the prime factors of a given positive integer.
;; Construct a flat list containing the prime factors in ascending order.
;; Example:
;; * (prime-factors 315)
;; (3 3 5 7)

(defun prime-factors (n)
  (labels
      ((aux (acc number factor)
	 (if (= factor number)
	     (cons factor acc)
	     (multiple-value-bind (quotient remainder) (floor number factor)
	       (cond ((and (zerop quotient)
			   (zerop remainder) (cons factor acc)))
		     ((zerop remainder) (aux (cons factor acc)
					     quotient
					     factor))
		     (t (aux acc number (1+ factor))))))))
    (nreverse (aux nil n 2))))

;; P36 (**) Determine the prime factors of a given positive integer (2).
;; Construct a list containing the prime factors and their multiplicity.
;; Example:
;; * (prime-factors-mult 315)
;; ((3 2) (5 1) (7 1))
;;
;; Hint: The problem is similar to problem P10.

(defun prime-factors-mult (n)
  (mapcar (lambda (x)
	    (list (second x) (first x)))
	  (encode (prime-factors n))))

;; P39 (*) A list of prime numbers.
;; Given a range of integers by its lower and upper limit, construct a list of all prime numbers in that range.

;; sieve of eratosthenes
(defun sieve (n m)
  ;; the double nil prefix makes each element's index equal to the integer it represents
  (let ((candidates (append (list nil nil) (range 2 m))))
    (do ((i 2 (1+ i)))
	((> i (sqrt m)) (remove-if-not #'identity (subseq candidates n)))
      (do ((j (* i i) (+ j i)))
	  ((> j m))
	(setf (elt candidates j) nil)))))

;; P40 (**) Goldbach's conjecture.
;; Goldbach's conjecture says that every positive even number greater than 2 is the sum of two prime numbers. Example: 28 = 5 + 23. It is one of the most famous facts in number theory that has not been proved to be correct in the general case. It has been numerically confirmed up to very large numbers (much larger than we can go with our Lisp system). Write a function to find the two prime numbers that sum up to a given even integer.
;;
;; Example:
;; * (goldbach 28)
;; (5 23)

(defun goldbach (n &optional primes)
  (when (and (> n 2)
	     (evenp n))
    (let ((candidates  (if primes primes
			   (sieve 2 (- n 2)))))
      (do* ((cs candidates (cdr cs))
	    (i (car cs) (car cs)))
	   ((null cs) nil)
	(do* ((ds cs (cdr ds))
	      (j (car ds) (car ds)))
	     ((null ds) nil)
	  (when (= (+ i j) n)
	    (return-from goldbach (list i j))))))))

;; P41 (**) A list of Goldbach compositions.
;; Given a range of integers by its lower and upper limit, print a list of all even numbers and their Goldbach composition.
;;
;; Example:
;; * (goldbach-list 9 20)
;; 10 = 3 + 7
;; 12 = 5 + 7
;; 14 = 3 + 11
;; 16 = 3 + 13
;; 18 = 5 + 13
;; 20 = 3 + 17
;;
;; In most cases, if an even number is written as the sum of two prime numbers, one of them is very small. Very rarely, the primes are both bigger than say 50. Try to find out how many such cases there are in the range 2..3000.
;;
;; Example (for a print limit of 50):
;; * (goldbach-list 1 2000 50)
;; 992 = 73 + 919
;; 1382 = 61 + 1321
;; 1856 = 67 + 1789
;; 1928 = 61 + 1867

(defun goldbach-list (n m &optional min)
  (let* ((primes (sieve 2 m))
	 (gs (mapcar
	      (lambda (x)
		(append (goldbach x (take-while primes
						(lambda (y) (< y x))))
			(list x)))
	      (remove-if-not (lambda (x)
			       (and (evenp x) (> x 2)))
			     (range n m))))
	 (hs (if min
		 (remove-if-not (lambda (x)
				  (and (> (first x) min)
				       (> (second x) min)))
				gs)
		 gs)))
    (dolist (item hs (length hs))
      (format t "~&~S = ~S + ~S "
	      (third item) (first item) (second item)))))

;; P54A (*) Check whether a given expression represents a binary tree
;; Write a function istree which returns true if and only if its argument is a list representing a binary tree.
;; Example:
;; * (istree '(a (b nil nil) nil))
;; T
;; * (istree '(a (b nil nil)))
;; NIL

(defun istree (maybe-tree)
  (match maybe-tree
    (nil t)
    ((list _ l r) (and (istree l) (istree r)))
    (t nil)))
